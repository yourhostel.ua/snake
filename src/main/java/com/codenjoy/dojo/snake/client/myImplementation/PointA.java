package com.codenjoy.dojo.snake.client.myImplementation;

import java.io.Serializable;

public record PointA(int x, int y) implements Serializable {

    public static PointA of(int x, int y) {
        return new PointA(x, y);
    }

    @Override
    public String toString() {
        return String.format("[%2d, %2d]", x, y);
    }

    public PointA move(int dx, int dy) {
        return new PointA(x + dx, y + dy);
    }

}
