package com.codenjoy.dojo.snake.client.myImplementation;

import com.codenjoy.dojo.services.Direction;
import com.codenjoy.dojo.services.Point;
import com.codenjoy.dojo.snake.client.Board;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MyClass {
    private final Lee lee;

    private MyClass(Board board) {
        this.lee = new Lee(15, 15, board);
    }

    public static String of(Board board){
        MyClass my = new MyClass(board);
        return my.go(board);
    }

    private String go(Board board) {
        PointA to;
        PointA toStone;
        PointA from = null;
        PointA cursor = null;
        List<PointA> obstacles = new ArrayList<>();

        if (board.getHead() != null) {

            from = PointA.of(board.getHead().getX(), board.getHead().getY());
            to = PointA.of(board.getApples().get(0).getX(), board.getApples().get(0).getY());
            toStone = PointA.of(board.getStones().get(0).getX(), board.getStones().get(0).getY());

            List<Point> snake = board.getSnake();

            snake.forEach(x -> obstacles.add(PointA.of(x.getX(), x.getY())));

            Optional<List<PointA>> trace = Optional.empty();

            obstacles.add(toStone);
            trace = lee.trace(from, to, obstacles);

            trace.ifPresent(path -> System.out.println(lee.boardFormatted(path)));

            if (trace.isEmpty()) {
                List<PointA> obs = new ArrayList<>(obstacles.stream().filter(x -> x != toStone).toList());
                lee.setField(new int[lee.getHeight()][lee.getWidth()]);
                trace = lee.trace(from, toStone, obs);

                Direction dir = board.getSnakeDirection();
                PointA next = checkDirect(from, dir);
                for (Point p : board.getWalls()) obs.add(PointA.of(p.getX(), p.getY()));

                if (obs.contains(next) && trace.isEmpty()) {
                    switch (dir) {
                        case UP -> {
                            return leftRight(from, obs, Direction.LEFT.toString());
                        }
                        case DOWN -> {
                            return leftRight(from, obs, Direction.RIGHT.toString());
                        }
                        case LEFT -> {
                            return upDown(from, obs, Direction.DOWN.toString());
                        }
                        case RIGHT -> {
                            return upDown(from, obs, Direction.UP.toString());
                        }
                    }
                }
            }

            if (trace.isPresent()) {
                if (trace.get().size() > 1)
                    cursor = PointA.of(trace.get().get(1).x(), trace.get().get(1).y());
            }
        }
        if (cursor != null) return steer(cursor, from);
        return Direction.STOP.toString();
    }

    private String leftRight(PointA from, List<PointA> obs, String _else) {
        if (obs.contains(checkDirect(from, Direction.LEFT))) return Direction.RIGHT.toString();
        if (obs.contains(checkDirect(from, Direction.RIGHT))) return Direction.LEFT.toString();
        return _else;
    }

    private String upDown(PointA from, List<PointA> obs, String _else) {
        if (obs.contains(checkDirect(from, Direction.UP))) return Direction.DOWN.toString();
        if (obs.contains(checkDirect(from, Direction.DOWN))) return Direction.UP.toString();
        return _else;
    }

    private PointA checkDirect(PointA head, Direction dir) {
        PointA p = null;
        switch (dir) {
            case UP -> p = PointA.of(head.x(), head.y() + 1);
            case DOWN -> p = PointA.of(head.x(), head.y() - 1);
            case LEFT -> p = PointA.of(head.x() - 1, head.y());
            case RIGHT -> p = PointA.of(head.x() + 1, head.y());
        }
        return p;
    }

    private String steer(PointA cursor, PointA from) {
        if (from.x() > cursor.x() && from.y() == cursor.y()) return Direction.LEFT.toString();
        if (from.x() < cursor.x() && from.y() == cursor.y()) return Direction.RIGHT.toString();
        if (from.y() > cursor.y() && from.x() == cursor.x()) return Direction.DOWN.toString();
        if (from.y() < cursor.y() && from.x() == cursor.x()) return Direction.UP.toString();
        return Direction.STOP.toString();
    }
}
