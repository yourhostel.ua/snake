package com.codenjoy.dojo.snake.client.myImplementation;

import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import com.codenjoy.dojo.services.Point;
import com.codenjoy.dojo.snake.client.Board;
import com.codenjoy.dojo.snake.client.myImplementation.colored.*;

public class Lee {
    private final int OBSTACLE = -9;
    private final int width;
    private final int height;
    private int[][] field;
    private final Board board;

    public Lee(int width, int height, Board board) {
        this.width = width;
        this.height = height;
        this.board = board;
        this.field = new int[height][width];
    }

    public int getHeight(){
        return this.height;
    }

    public int getWidth(){
        return this.width;
    }

    public void setField(int[][] field) {
        this.field = field;
    }

    private int get(int x, int y) {
        return field[y][x];
    }

    private void set(int x, int y, int value) {
        field[y][x] = value;
    }

    private int get(PointA point) {
        return get(point.x(), point.y());
    }

    private void set(PointA point, int value) {
        set(point.x(), point.y(), value);
    }

    private boolean isOnBoard(PointA point) {
        return point.x() >= 1 && point.x() < width - 1 &&
                point.y() >= 1 && point.y() < height - 1;
    }

    private boolean isUnvisited(PointA point) {
        int EMPTY = 0;
        return get(point) == EMPTY;
    }

    private Supplier<Stream<PointA>> deltas() {
        return () -> Stream.of(
                PointA.of(-1, 0), // offset, not a point
                PointA.of(1, 0),  // offset, not a point
                PointA.of(0, 1),  // offset, not a point
                PointA.of(0, -1)  // offset, not a point
        );
    }

    private Stream<PointA> neighbours(PointA point) {
        return deltas().get()
                .map(d -> point.move(d.x(), d.y()))
                .filter(this::isOnBoard);
    }

    private Stream<PointA> neighboursUnvisited(PointA point) {
        return neighbours(point)
                .filter(this::isUnvisited);
    }

    private List<PointA> neighboursByValue(PointA point, int value) {
        return neighbours(point)
                .filter(p -> get(p) == value)
                .toList();
    }

    private void initializeBoard(List<PointA> obstacles) {
        obstacles.forEach(p -> set(p, OBSTACLE));
    }

    public Optional<List<PointA>> trace(PointA start, PointA finish, List<PointA> obstacles) {
        // 1. initialization
        int START = 1;
        initializeBoard(obstacles);
        int[] counter = {START}; // HEAP due to lambda
        set(start, counter[0]);
        counter[0]++;
        boolean found = false;
        // 2. fill the board
        for (Set<PointA> curr = new HashSet<>(Set.of(start)); !(found || curr.isEmpty()); counter[0]++) {
            Set<PointA> next = curr.stream()
                    .flatMap(this::neighboursUnvisited)
                    .collect(Collectors.toSet());

            next.forEach(p -> set(p, counter[0]));
            found = next.contains(finish);
            curr = next;
        }
        // 3. backtrace (reconstruct the path)
        if (!found) return Optional.empty();
        LinkedList<PointA> path = new LinkedList<>();
        path.add(finish);
        counter[0]--;

        PointA curr = finish;
        while (counter[0] > START) {
            counter[0]--;
            PointA prev = neighboursByValue(curr, counter[0]).get(0);
            if (prev == start) break;
            path.addFirst(prev);
            curr = prev;
        }
        return Optional.of(path);
    }

    public String cellFormatted(PointA p, List<PointA> path) {
        int value = get(p);
        String valueF = String.format("%3d", value);
        Point apple = board.getApples().get(0);

        if (p.x() == apple.getX() && p.y() == apple.getY()) {
            Attribute a = new Attribute(Ansi.ColorFont.YELLOW);
            return Colored.build(" XX", a);
        }

        if (value == OBSTACLE) {
            Attribute a = new Attribute(Ansi.ColorFont.BLUE);
            return Colored.build(" XX", a);
        }

        if (path.isEmpty()) return valueF;

        if (path.contains(p)) {
            Attribute a = new Attribute(Ansi.ColorFont.RED);
            return Colored.build(valueF, a);
        }

        return valueF;//" --";
    }

    public String boardFormatted(List<PointA> path) {
        return IntStream.range(0, height).mapToObj(y ->
                        IntStream.range(0, width)
                                .mapToObj(x -> PointA.of(x, y))
                                .map(p -> cellFormatted(p, path))
                                .collect(Collectors.joining())
                )
                .collect(Collectors.joining("\n"));
    }

    @Override
    public String toString() {
        return boardFormatted(Collections.emptyList());
    }
}
